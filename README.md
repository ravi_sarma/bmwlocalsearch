# README #

* Personal code 
* Developed this for 3 hour coding challenge. 
* Follows the Single Activitiy (controller) and multiple fragment (ui) model

### What is this repository for? ###
* Local Search Android app
* Connects to REST API and shows local search results.
* Overflow menu has Sort by Name, ETA, Distance
* MapView with location, navigation details
* Works on Android JB, KK, L and M (permissions)

### Screenshots ###
![search.png](https://bitbucket.org/repo/BqA4xK/images/1312212726-search.png)

### How do I get set up? ###

* Do a gradle sync

### TODO ###

* Junit/Espresso Tests still pending

### Who do I talk to? ###

* sarma.m.ravi@gmail.com