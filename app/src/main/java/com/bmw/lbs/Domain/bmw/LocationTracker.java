package com.bmw.lbs.Domain.bmw;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;


/**
 * Created by rsarn on 8/10/15.
 */
public class LocationTracker implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public static String KEYLAT = "keylat";
    public static String KEYLON = "keylon";
    private GoogleApiClient googleAPIClient = null;
    private LocationRequest mLocationRequest = null;
    private Context mContext = null;
    private Context mDialogContext = null;
    private Looper mLooper = null;
    private LocationManager mLocationManager;
    private final SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Location prevLocation;
    private final int NUM_UPDATES = 5;

    public LocationTracker(Context ctx, Context mDialogCtx, Looper looper) {
        mContext = ctx;
        mDialogContext = mDialogCtx;
        mLocationManager = ((LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE));
        sharedPreferences = ctx.getSharedPreferences("BMW_LBS_PREF", Context.MODE_PRIVATE);
        mLooper = looper;
    }

    public void start() {
        buildAPIClient();
        createLocationRequest();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setNumUpdates(NUM_UPDATES);
    }


    public void buildAPIClient() {
        if (googleAPIClient == null) {
            googleAPIClient = new GoogleApiClient.Builder(mContext).addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
        }
        googleAPIClient.connect();
    }

    /*
     * Called by Location Services when the request to connect the client
     * finishes successfully. At this point, you can request the current
     * location or start periodic updates
     */
    @Override
    public void onConnected(Bundle dataBundle) {
        checkLocationSettings();
    }

    public void registerLocationRequest() {
        if (googleAPIClient != null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleAPIClient, mLocationRequest, this, mLooper);
            editor = sharedPreferences.edit();
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        // Get the most accurate location
        if (!(prevLocation != null && location.getAccuracy() <= prevLocation.getAccuracy())) {
            editor.putLong(KEYLAT, Double.doubleToLongBits(location.getLatitude()));
            editor.putLong(KEYLON, Double.doubleToLongBits(location.getLongitude()));
            editor.commit();
        }
        prevLocation = location;
    }

    public void stop() {
        if (googleAPIClient != null) {
            if (googleAPIClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(googleAPIClient, this);
            }
            /*
          * After disconnect() is called, the client is considered "dead".
          */
            googleAPIClient.disconnect();
            googleAPIClient = null;
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }


    public void checkLocationSettings() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleAPIClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        registerLocationRequest();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    (Activity) mDialogContext, 500);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        } catch (NullPointerException nex) {

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        stop();
                        break;
                }
            }
        });
    }
}
