package com.bmw.lbs.Domain.Imgur.Model;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class GalleryResponse extends BaseResponse {
    @NonNull
    public List<ImgurBaseObject> data = new ArrayList<>();
}