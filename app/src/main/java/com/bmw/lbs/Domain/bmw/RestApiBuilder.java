package com.bmw.lbs.Domain.bmw;

import android.util.Log;

import com.activeandroid.query.Delete;
import com.bmw.lbs.Model.LocationResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.Callback;

/**
 * Created by rsarn on 1/24/16.
 */
public class RestApiBuilder {

    private static String ENDPOINT = "http://localsearch.azurewebsites.net/api/";

    public static RestInterface build() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(RestInterface.class);
    }

    public static void fetchLocations(final RequestStatusInterface callback) {
        RestInterface networkInterface = RestApiBuilder.build();
        Call<List<SearchResult>> results = networkInterface.getLocalLocations();
        results.enqueue(new Callback<List<SearchResult>>() {
            @Override
            public void onResponse(Response<List<SearchResult>> response) {
                if (response.isSuccess()) {
                    Log.i("LocationService", "fetch success");
                    List<SearchResult> resultList = response.body();
                    if (resultList.size() > 0) {
                        // Update the local cache
                        new Delete().from(LocationResult.class).execute();
                        for (SearchResult result : resultList) {
                            new LocationResult(result.getID(), result.getName(), result.getLatitude(), result.getLongitude(), result.getArrivalTime(), result.getAddress(), 0).save();
                        }
                    }
                    callback.onRequestComplete();
                } else {
                    callback.onRequestFail();
                    Log.i("LocationService", "fetch fail");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onRequestFail();
                Log.e("LocationService", "fetch fail " + t.getMessage());
            }
        });
    }
}
