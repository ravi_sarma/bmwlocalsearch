package com.bmw.lbs.Domain.Imgur.Network;

import android.text.TextUtils;
import android.util.Log;

import com.bmw.lbs.BuildConfig;
import com.bmw.lbs.Domain.Imgur.Model.ImgurBaseObject;
import com.bmw.lbs.LocalSearchApplication;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * Created by ccethub on 1/29/16.
 */
public class ImgurAPI {

    private static final String API_URL = "https://api.imgur.com";
    private static ImgurService imgurService;
    private static long CACHE_SIZE = 1024 * 1024 * 10;

    public static ImgurService getService() {
        if (imgurService == null) {
            Retrofit retrofit = new Retrofit.Builder().baseUrl(API_URL).client(getClient()).addConverterFactory(getConverter()).build();
            imgurService = retrofit.create(ImgurService.class);
        }
        return imgurService;
    }

    private static OkHttpClient getClient() {
        OkHttpClient client = new OkHttpClient();
        OkHttpClient.Builder builder = client.newBuilder();
        builder.connectTimeout(20, TimeUnit.SECONDS);
        builder.interceptors().add(new SimpleOAuthInterceptor());
        File cache = LocalSearchApplication.getInstance().getCacheDir();
        if (cache != null && cache.exists()) {
            File httpCache = new File(cache, "http_cache");
            builder.cache(new Cache(httpCache, CACHE_SIZE));
        }
        return client;
    }


    private static GsonConverterFactory getConverter() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(ImgurBaseObject.class, new ImgurSerializer())
                .create();

        return GsonConverterFactory.create(gson);
    }


}
