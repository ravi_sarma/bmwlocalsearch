package com.bmw.lbs.Domain.bmw;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


/**
 * Created by rsarn on 1/23/16.
 */
public interface RestInterface {

    @GET("Locations")
    Call<List<SearchResult>> getLocalLocations();
}
