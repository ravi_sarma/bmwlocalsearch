package com.bmw.lbs.Domain.bmw;

/**
 * Created by rsarn on 1/24/16.
 */
public interface RequestStatusInterface {

    public void onRequestComplete();
    public void onRequestFail();
}
