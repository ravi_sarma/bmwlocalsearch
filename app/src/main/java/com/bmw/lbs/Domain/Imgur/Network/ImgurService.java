package com.bmw.lbs.Domain.Imgur.Network;

import com.bmw.lbs.Domain.Imgur.Model.GalleryResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ImgurService {
    // Get Requests
    @GET("/3/gallery/{section}/{sort}/{page}")
    Call<GalleryResponse> getGallery(@Path("section") String section, @Path("sort") String sort, @Path("page") int page, @Query("showViral") boolean showViral);
}