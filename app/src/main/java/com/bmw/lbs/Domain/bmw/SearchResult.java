package com.bmw.lbs.Domain.bmw;

import com.activeandroid.annotation.Column;

/**
 * Created by rsarn on 1/25/16.
 */
public class SearchResult {

    private long ID;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public String getArrivalTime() {
        return ArrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        ArrivalTime = arrivalTime;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    private String Name;
    private double Latitude;
    private double Longitude;
    private String ArrivalTime;
    private String Address;


    public SearchResult(long ID, String name, double latitude, double longitude, String arrivalTime, String address) {
        this.ID = ID;
        Name = name;
        Latitude = latitude;
        Longitude = longitude;
        ArrivalTime = arrivalTime;
        Address = address;
    }
}
