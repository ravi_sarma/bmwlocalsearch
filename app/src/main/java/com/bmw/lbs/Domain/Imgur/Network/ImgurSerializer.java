package com.bmw.lbs.Domain.Imgur.Network;

import com.bmw.lbs.Domain.Imgur.Model.ImgurBaseObject;
import com.bmw.lbs.Domain.Imgur.Model.ImgurPhoto;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class ImgurSerializer implements JsonDeserializer<ImgurBaseObject> {
    private final Gson gson = new GsonBuilder().create();

    @Override
    public ImgurBaseObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        boolean isAlbum = object.has("images_count") && object.get("images_count").getAsInt() > 0;
        ImgurBaseObject obj = gson.fromJson(json, isAlbum ? null: ImgurPhoto.class);

        // Need to manually check if the up/down votes are set to null as GSON will initialize it to 0
        if (object.has("ups") && object.has("downs")) {
            boolean hasUpVotes = !object.get("ups").isJsonNull();
            boolean hasDownVotes = !object.get("downs").isJsonNull();
            obj.setIsListed(hasUpVotes && hasDownVotes);
        } else {
            obj.setIsListed(false);
        }

        return obj;
    }
}