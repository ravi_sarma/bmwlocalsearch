package com.bmw.lbs.Domain.Imgur.Network;

import android.util.Log;

import com.bmw.lbs.BuildConfig;

import java.io.IOException;
import java.net.HttpURLConnection;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by ccethub on 1/29/16.
 */

public class SimpleOAuthInterceptor implements Interceptor {
    static final String CLIENT_ID = BuildConfig.API_CLIENT_ID;

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder authRequest = request.newBuilder();
        authRequest.addHeader("Authorization", getAuthHeader());
        Request newRequest = authRequest.method(request.method(), request.body()).build();
        Response response = chain.proceed(newRequest);
        if (response.code() == HttpURLConnection.HTTP_UNAUTHORIZED || response.code() == HttpURLConnection.HTTP_FORBIDDEN) {
            Log.e("IMGUR", "oauth err");
        }
        return response;
    }

    private String getAuthHeader() {
        return "Client-ID " + CLIENT_ID;
    }
}
