package com.bmw.lbs.Domain.bmw;

import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bmw.lbs.Model.LocationResult;
import com.bmw.lbs.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by rsarn on 1/23/16.
 */
public class LocationResultsAdapter extends RecyclerView.Adapter {

    List<LocationResult> locationResultList;
    OnItemSelectListener mListener;

    public interface OnItemSelectListener{
        public void onItemClicked(long position);
    }

    public LocationResultsAdapter(List result, OnItemSelectListener listener) {
        locationResultList = result;
        mListener = listener;
    }

    public static final class LocationItemHolder extends RecyclerView.ViewHolder {
        TextView locationResult;
        TextView locationETA;
        TextView locationAddress;

        LocationItemHolder(View itemView) {
            super(itemView);
            locationResult = (TextView) itemView.findViewById(R.id.locationName);
            locationETA = (TextView) itemView.findViewById(R.id.locationETA);
            locationAddress = (TextView) itemView.findViewById(R.id.locationAddress);
        }

    }

    public void setLocationResultList(List<LocationResult> locationResultList) {
        this.locationResultList = locationResultList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView  = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_result_item, parent, false);
        LocationItemHolder viewHolder = new LocationItemHolder(inflatedView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        LocationItemHolder viewHolder = (LocationItemHolder) holder;
        viewHolder.locationResult.setText(locationResultList.get(position).Name);
        viewHolder.locationETA.setText("ETA : " + convertDate(locationResultList.get(position).ArrivalTime));
        viewHolder.locationAddress.setText(locationResultList.get(position).Address);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Tell the Fragment
                mListener.onItemClicked(locationResultList.get(position).ID);
            }
        });
    }

    @Override
    public int getItemCount() {
        return locationResultList.size();
    }


    private static String convertDate(String milSec) {
        String retDate = null;
        if(milSec!=null) {
            String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS";
            try {
                retDate = new SimpleDateFormat("MM-dd h:mm a").format(new SimpleDateFormat(pattern).parse(milSec));
            } catch (ParseException tex) {
                // return null string
            }
        }
        return retDate;
    }
}
