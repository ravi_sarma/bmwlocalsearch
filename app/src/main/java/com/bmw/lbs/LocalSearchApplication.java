package com.bmw.lbs;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

/**
 * Created by rsarn on 1/23/16.
 */
public class LocalSearchApplication extends Application {

    static LocalSearchApplication appInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        appInstance = this;
        ActiveAndroid.initialize(this);
    }

    public static LocalSearchApplication getInstance() {
        return appInstance;
    }
}
