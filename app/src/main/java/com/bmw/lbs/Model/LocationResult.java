package com.bmw.lbs.Model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by rsarn on 1/23/16.
 */

@Table(name = "LocationResult")
public class LocationResult extends Model {

    @Column(name = "uid", index = true)
    public long ID;
    @Column(name = "name")
    public String Name;
    @Column(name = "lat")
    public double Latitude;
    @Column(name = "lon")
    public double Longitude;
    @Column(name = "eta")
    public String ArrivalTime;
    @Column(name = "address")
    public String Address;
    @Column(name = "distance")
    public double Distance;

    public LocationResult() {
        super();
    }

    public LocationResult(long ID, String name, double latitude, double longitude, String arrivalTime, String address, double distance) {
        this.ID = ID;
        Name = name;
        Latitude = latitude;
        Longitude = longitude;
        ArrivalTime = arrivalTime;
        Address = address;
        Distance = distance;
    }

    public void setDistance(double distance) {
        Distance = distance;
    }
}
