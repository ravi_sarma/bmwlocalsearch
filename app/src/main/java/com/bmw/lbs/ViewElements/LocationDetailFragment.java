package com.bmw.lbs.ViewElements;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.bmw.lbs.Domain.bmw.LocationTracker;
import com.bmw.lbs.Model.LocationResult;
import com.bmw.lbs.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import butterknife.Bind;
import butterknife.ButterKnife;


public class LocationDetailFragment extends Fragment {

    public static String DATA_KEY = "key";

    @Bind(R.id.fragmentToolBar)
    public Toolbar mToolbar;

    @Bind(R.id.mapView)
    public MapView mMapView;

    @Bind(R.id.placeNameTextView)
    public TextView placeNameTextView;

    @Bind(R.id.placeAddressTextView)
    public TextView placeAddressTextView;

    @Bind(R.id.distanceTextView)
    public TextView distanceTextView;

    @Bind(R.id.etaTextView)
    public TextView etaTextView;

    private GoogleMap mGoogleMap;
    private long mPrimaryKey;
    private LocationResult mLocationResult;
    private Marker mMarker;
    private int mZoomLevel = 15;
    private SharedPreferences mSharedPreference;
    private static final double TOMILE = 0.0006214;

    public LocationDetailFragment() {
        // Required empty public constructor
    }

    public static LocationDetailFragment getInstance(long dataKey) {
        LocationDetailFragment detailFragment = new LocationDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(DATA_KEY, dataKey);
        detailFragment.setArguments(bundle);
        return detailFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.getArguments() != null) {
            mPrimaryKey = getArguments().getLong(DATA_KEY, 0);
        }
        this.setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflatedView = inflater.inflate(R.layout.fragment_location_detail, container, false);
        ButterKnife.bind(this, inflatedView);

        // Set up Toolbar
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(mToolbar);
        appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appCompatActivity.getSupportActionBar().setDisplayShowTitleEnabled(true);
        appCompatActivity.getSupportActionBar().setTitle(getContext().getResources().getString(R.string.location_detail));

        mSharedPreference = getContext().getSharedPreferences("BMW_LBS_PREF", Context.MODE_PRIVATE);

        // fetch data from cache to show
        if (mPrimaryKey > 0) {
            queryLocalCache();
        }

        // setup map
        setupMap(savedInstanceState);

        return inflatedView;
    }

    // Orientation change save state
    public void onSaveInstanceState(Bundle outState){
        //This MUST be done before saving any of your own or your base class's variables
        final Bundle mapViewSaveState = new Bundle(outState);
        mMapView.onSaveInstanceState(mapViewSaveState);
        outState.putBundle("mapViewSaveState", mapViewSaveState);
        //Add any other variables here.
        super.onSaveInstanceState(outState);
    }

    public void setupMap(Bundle savedInstanceState) {
        final Bundle mapViewSavedInstanceState = savedInstanceState != null ? savedInstanceState.getBundle("mapViewSaveState") : savedInstanceState;
        mMapView.onCreate(mapViewSavedInstanceState);
        mMapView.onResume();//needed to get the map to display immediately
        MapsInitializer.initialize(getContext());

        mGoogleMap = mMapView.getMap();
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setCompassEnabled(false);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        if (mLocationResult != null) {
            LatLng latLng = new LatLng(mLocationResult.Latitude, mLocationResult.Longitude);
            addMarker(latLng);
            placeNameTextView.setText(mLocationResult.Name);
            distanceTextView.setText("Distance : " + String.format("%.2f", findDistance()) + " miles");
            placeAddressTextView.setText(mLocationResult.Address);
            etaTextView.setText("Arrival Time : " + convertDate(mLocationResult.ArrivalTime));
        }
    }

    private double findDistance() {
        float result[] = new float[3];
        double startLat = Double.longBitsToDouble(mSharedPreference.getLong(LocationTracker.KEYLAT, 0));
        double startLon = Double.longBitsToDouble(mSharedPreference.getLong(LocationTracker.KEYLON, 0));
        Location.distanceBetween(startLat, startLon, mLocationResult.Latitude, mLocationResult.Longitude, result);
        return (result[0] * TOMILE);
    }

    public void addMarker(LatLng latLng) {
        if (mGoogleMap != null) {
            mMarker = mGoogleMap.addMarker(new MarkerOptions().position(latLng));
            animateToLatLongPoint(latLng, mZoomLevel);
        }
    }

    public void animateToLatLongPoint(LatLng latLng, int zoomLevel) {
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    private void queryLocalCache() {
        mLocationResult = new Select().from(LocationResult.class).where("uid = " + mPrimaryKey).executeSingle();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private static String convertDate(String milSec) {
        String retDate = null;
        String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS";
        try {
            retDate = new SimpleDateFormat("HH:mm:ss a").format(new SimpleDateFormat(pattern).parse(milSec));
        } catch (ParseException tex) {
            // return null string
        }
        return retDate;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return super.onOptionsItemSelected(menuItem);
    }
}
