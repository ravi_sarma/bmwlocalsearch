package com.bmw.lbs.ViewElements;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bmw.lbs.Domain.bmw.LocationTracker;
import com.bmw.lbs.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, LocationResultsFragment.OnFragmentInteractionListener {

    @Bind(R.id.drawer_layout)
    public DrawerLayout mDrawer;

    @Bind(R.id.nav_view)
    public NavigationView mNavigationView;

    @Bind(R.id.fragmentContainer)
    public RelativeLayout mFragmentContainer;

    Fragment mCurrentFragment = null;
    LocationResultsFragment mLocationResultsFragment;
    LocationTracker mLocationTracker;
    private static final int PERMISSION_CODE = 005;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        //Set up fragment
        mLocationResultsFragment = new LocationResultsFragment();
        drawFragment(mLocationResultsFragment);
        mLocationTracker = new LocationTracker(this, this, null);
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    private void drawFragment(Fragment fragmentToDraw) {
        if (fragmentToDraw != mCurrentFragment && !fragmentToDraw.isAdded()) {
            mCurrentFragment = fragmentToDraw;
            FragmentManager fragmentManager = this.getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(android.R.anim.fade_in, R.anim.slide_out_right, R.anim.slide_in_left, R.anim.slide_out_left);
            transaction.replace(R.id.fragmentContainer, fragmentToDraw);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        } else {
            mLocationTracker.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mLocationTracker.stop();
    }

    @Override
    public void onSearchResultClick(long key) {
        // Launch the Detail screen
        LocationDetailFragment detailFragment = LocationDetailFragment.getInstance(key);
        drawFragment(detailFragment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        /*if (id == R.id.action_settings) {
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_places) {
            if (!(mCurrentFragment instanceof LocationResultsFragment)) {
                drawFragment(new LocationResultsFragment());
            }
        } else if (id == R.id.nav_manage) {
            showSnackBarMessage("Snap, still under construction!", ContextCompat.getColor(this, R.color.green), Color.WHITE);
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
                return;
            }
            super.onBackPressed();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (!(mCurrentFragment instanceof LocationResultsFragment)) {
            //This method is called when the up button is pressed. Just the pop back stack.
            FragmentManager fm = getSupportFragmentManager();
            if (fm.getBackStackEntryCount() > 1) {
                fm.popBackStackImmediate();
            }
        }
        return true;
    }

    private void showSnackBarMessage(String message, @ColorInt int backgroundColor, @ColorInt int textColor) {
        Snackbar snackbar = Snackbar.make(mFragmentContainer, message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(backgroundColor);

        TextView snackBarText = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        snackBarText.setTextColor(textColor);
        snackbar.show();
    }

    /**
     * LOCATION SETTINGS AND MARSHMALLOW PERMISSIONS
     */

    @TargetApi(Build.VERSION_CODES.M)
    public void checkLocationPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {
                // No explanation needed, we can request the permission.
                requestPermissions(new String[]{/*Manifest.permission.READ_PHONE_STATE, */Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSION_CODE);
            }
        } else {
            // permission was granted, yay! Do the stuff
            mLocationTracker.start();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the stuff
                    mLocationTracker.start();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }
}
