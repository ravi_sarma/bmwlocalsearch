package com.bmw.lbs.ViewElements;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.bmw.lbs.Domain.bmw.LocationResultsAdapter;
import com.bmw.lbs.Domain.bmw.LocationTracker;
import com.bmw.lbs.Domain.bmw.RestApiBuilder;
import com.bmw.lbs.Domain.bmw.RequestStatusInterface;
import com.bmw.lbs.Model.LocationResult;
import com.bmw.lbs.R;

import java.util.List;
import java.util.concurrent.Executors;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LocationResultsFragment extends Fragment implements RequestStatusInterface, LocationResultsAdapter.OnItemSelectListener {


    @Bind(R.id.innertoolbar)
    public Toolbar mToolbar;

    @Bind(R.id.recyclerView)
    public RecyclerView mRecyclerView;

    @Bind(R.id.collapsingToolbar)
    public CollapsingToolbarLayout mCollapsingToolbarLayout;

    @Bind(R.id.noResultTextView)
    TextView noResultsTextView;

    LocationResultsAdapter recyclerViewAdapter;
    String sortType = "name";
    private OnFragmentInteractionListener mListener;
    private static final double TOMILE = 0.0006214;

    public LocationResultsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);
        // Rest API call. Retrofit takes care of executing request on background thread
        // and respond on Main Thread.
        RestApiBuilder.fetchLocations(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflatedView = inflater.inflate(R.layout.fragment_location_result, container, false);
        ButterKnife.bind(this, inflatedView);

        // Set up Toolbar
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        appCompatActivity.setSupportActionBar(mToolbar);
        appCompatActivity.getSupportActionBar().setDisplayShowTitleEnabled(true);
        appCompatActivity.getSupportActionBar().setTitle(getContext().getResources().getString(R.string.app_name));

        mCollapsingToolbarLayout.setTitleEnabled(false);
        // Cook the burger icon
        DrawerLayout drawer = ((MainActivity) getActivity()).mDrawer;
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        setupRecycler();
        return inflatedView;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshAdapter();
    }

    private void setupRecycler() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewAdapter = new LocationResultsAdapter(queryLocalCache(sortType), this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(recyclerViewAdapter);
        recyclerViewAdapter.notifyDataSetChanged();
    }

    private void refreshAdapter() {
        if (recyclerViewAdapter != null) {
            recyclerViewAdapter.setLocationResultList(queryLocalCache(sortType));
            recyclerViewAdapter.notifyDataSetChanged();
            mRecyclerView.invalidate();
        }
    }

    private List queryLocalCache(String sortType) {
        List<LocationResult> locationResultList = null;
        locationResultList = new Select().from(LocationResult.class).orderBy(sortType).execute();
        if (mRecyclerView != null) {
            if (locationResultList == null || locationResultList.size() == 0) {
                mRecyclerView.setVisibility(View.GONE);
                noResultsTextView.setVisibility(View.VISIBLE);
            } else {
                mRecyclerView.setVisibility(View.VISIBLE);
                noResultsTextView.setVisibility(View.GONE);
            }
        }
        return locationResultList;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRequestComplete() {
        // refresh the UI
        refreshAdapter();
        // find distance from current location on background thread
        Executors.newSingleThreadExecutor().execute(computeDistance);
        Log.i("bmwlbs", "request complete");
    }

    @Override
    public void onRequestFail() {

    }

    @Override
    public void onItemClicked(long position) {
        // Tell the Activity
        mListener.onSearchResultClick(position);
    }

    Runnable computeDistance = new Runnable() {
        @Override
        public void run() {
            List<LocationResult> locationResultList = new Select().from(LocationResult.class).execute();
            SharedPreferences sharedPreference = getContext().getSharedPreferences("BMW_LBS_PREF", Context.MODE_PRIVATE);
            double startLat = Double.longBitsToDouble(sharedPreference.getLong(LocationTracker.KEYLAT, 0));
            double startLon = Double.longBitsToDouble(sharedPreference.getLong(LocationTracker.KEYLON, 0));
            for (LocationResult result : locationResultList) {
                Log.i("bmwlbs", "computing distance");
                result.setDistance(findDistance(sharedPreference, result, startLat, startLon));
                result.save();
            }
        }
    };

    private double findDistance(SharedPreferences sharedPreference, LocationResult locationResult, double startLat, double startLon) {
        float result[] = new float[3];
        Location.distanceBetween(startLat, startLon, locationResult.Latitude, locationResult.Longitude, result);
        return (result[0] * TOMILE);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onSearchResultClick(long key);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_sort_name:
                sortType = "name";
                break;
            case R.id.action_sort_eta:
                sortType = "eta";
                break;
            case R.id.action_sort_distance:
                sortType = "distance";
                break;
        }
        refreshAdapter();
        return super.onOptionsItemSelected(item);
    }

}
